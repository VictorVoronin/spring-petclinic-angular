FROM nginx:1.17.8

ADD default.conf /etc/nginx/conf.d/

WORKDIR /usr/share/nginx/html/petclinic/dist

ADD dist ./